#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

template <typename... Bases>
struct DerivedAfter : Bases...
{
};

class Car
{};

class Boat
{};

using Amphibian = DerivedAfter<Car, Boat>;

DerivedAfter<> it_works;

///////////////////////////////////////////////////////////////////////////////
/// Head-Tail for class templates
///
template <typename... Types>
struct Count;

// partial specialization - Head-Tail
template <typename Head, typename... Tail>
struct Count<Head, Tail...>
{
    constexpr static int value = 1 + Count<Tail...>::value;
};

// final specialization - ends recursion
template <>
struct Count<>
{
    const static int value = 0;
};

template <typename... T>
struct TestCount
{
    static_assert(Count<T...>::value == sizeof...(T), "Error");
};

TEST_CASE("head tail idiom")
{
    static_assert(Count<int, double, string>::value == 3, "Error");

    TestCount<> t0;
    TestCount<int, double, string> t3;
}

///////////////////////////////////////////////////////////////////////////////
/// Head-Tail for function templates
///
void print() {}

template <typename Head, typename... Tail>
void print(const Head& head, const Tail&... tail)
{
    cout << head << " ";
    print(tail...);
}

TEST_CASE("printing variadic args")
{
    print(1, 3.14, "test");
    print(1, 3.14, "test", 665LL, "text"s);
}
