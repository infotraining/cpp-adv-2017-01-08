#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <mutex>
#include <memory>

using namespace std;
using namespace Catch::Matchers;

class Gadget
{
    int id_ = -1;
    string name_ = "unknown";
public:
    Gadget(int id,const string& name) :id_{id}, name_{name}
    {
        cout << "Gadget(" << id << ", " << name_ << ")" << endl;
    }

    Gadget() = default;

    virtual ~Gadget()
    {
        cout << "~Gadget(" << id_ << ", " << name_ << ")" << endl;
    }

    virtual void info() const
    {
        cout << "Gadget(" << id_ << ", " << name_ << ")\n";
    }

    virtual void use() const
    {
        cout << "using Gadget(" << id_ << ", " << name_ << ")\n";
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

namespace Legacy
{
    Gadget* get_gadget(const string& name)
    {
        static int id;
        return new Gadget(++id, name);
    }

    void use(Gadget* g)
    {
        if (g)
            g->use();
    }

    Gadget* create_gadgets()
    {
        Gadget* tab = new Gadget[10];
        return tab;
    }
}

void use(std::unique_ptr<Gadget> g)
{
    if (g)
        g->use();
}

std::unique_ptr<Gadget> get_gadget(const string& name)
{
    static int id;
    return std::make_unique<Gadget>(++id, name);
}

TEST_CASE("leaky code")
{
    Legacy::get_gadget("g1");

    Legacy::use(Legacy::get_gadget("g2"));
}

TEST_CASE("unique_pointer - leak-free code")
{
    get_gadget("ug1");
    auto ug2 = get_gadget("ug2");
    Legacy::use(ug2.get());

    use(get_gadget("ug3"));
    use(move(ug2));

    unique_ptr<Gadget[]> gadgets{Legacy::create_gadgets()};
    gadgets[0].use();
}

TEST_CASE("decltype")
{
    auto gadget_by_name_comp = [](const auto& g1, const auto& g2) {
        return g1->name() < g2->name();
    };

    set<std::unique_ptr<Gadget>, decltype(gadget_by_name_comp)> gadgets{gadget_by_name_comp};
    decltype(gadgets) gadgets2{gadget_by_name_comp};

    gadgets.insert(get_gadget("ipad"));
    gadgets.insert(get_gadget("smartphone"));
    gadgets.insert(get_gadget("mp3 player"));

    cout << "\nGadgets:\n";
    for(const auto& g : gadgets)
        g->info();
}

TEST_CASE("auto traps")
{
    const int x = 10;
    const int& ref_x = x;

    auto y = ref_x;
    auto& l = x;
    auto& z = ref_x;

    vector<string> words = { "one", "two", "three" };

    for(const auto& w : words)
        cout << w << " ";
    cout << endl;

    map<int, string> dict = { {1, "one"}, {2, "two"} };

    decltype(dict[0]) item = dict[1];
}
