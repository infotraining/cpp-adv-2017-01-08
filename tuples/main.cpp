#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <algorithm>
#include <numeric>

using namespace std;
using namespace Catch::Matchers;

template <typename T>
auto calc_stats(const vector<T>& data)
{
    typename vector<T>::const_iterator min_it, max_it;
    tie(min_it, max_it) = minmax_element(data.begin(), data.end());
    double avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

    return make_tuple(*min_it, *max_it, avg);
}

TEST_CASE("tuples")
{
    tuple<int, double, string> t1{42, 3.14, "test"};

    REQUIRE(get<0>(t1) == 42);
    REQUIRE(get<1>(t1) == 3.14);
    REQUIRE(get<2>(t1) == "test");

    SECTION("tuples with refs")
    {
        int x = 10;
        double y = 3.14;

        tuple<int&, double&> reft{x, y};

        REQUIRE(get<0>(reft) == 10);
        get<0>(reft) = 1;
        REQUIRE(x == 1);

        reft = make_tuple(13, 13.13);

        REQUIRE(x == 13);
        REQUIRE(y == Approx(13.13));
    }
}

TEST_CASE("calculating stats")
{
    vector<int> vec = { 1, 7, 342, 12, 134, 665 };

    SECTION("C++11")
    {
        int min, max;
        double avg;

        tie(min, max, avg) = calc_stats(vec);

        cout << "min: " << min << endl;
        cout << "max: " << max << endl;
        cout << "avg: " << avg << endl;
    }

    SECTION("C++17")
    {
//        auto[min, max, avg] = calc_stats(vec);
//        cout << "min: " << min << endl;
//        cout << "max: " << max << endl;
//        cout << "avg: " << avg << endl;
    }
}

struct X
{
    int a;
    double b;
    string s;

    auto tied() const
    {
        return tie(a, b, s);
    }

    bool operator<(const X& other) const
    {
        return tied() < other.tied();
    }

    bool operator==(const X& other) const
    {
        return tied() == other.tied();
    }
};

//template <size_t... Ns>
//struct Indexes
//{};

template <typename... Ts, size_t... Ns>
auto cherry_pick(const tuple<Ts...>& t, std::index_sequence<Ns...>)
{
    return make_tuple(get<Ns>(t)...);
}

TEST_CASE("cherry picking for tuple")
{
    auto t1 = make_tuple(1, 3.14, 665, "text");

    auto t2 = cherry_pick(t1, std::index_sequence<0, 2>());

    REQUIRE(get<0>(t2) == 1);
    REQUIRE(get<1>(t2) == 665);
}

namespace details
{
    template <typename Func, typename T, size_t... Is>
    void tuple_apply_impl(Func f, T t, index_sequence<Is...>)
    {
        initializer_list<bool>{ (f(get<Is>(t)), false)... };
    }
}

template<typename Func, typename... Ts>
void tuple_apply(Func f, const tuple<Ts...>& t)
{
    using Indexes = make_index_sequence<sizeof...(Ts)>;

    details::tuple_apply_impl(f, t, Indexes{});
}


TEST_CASE("foreach for tuples")
{
    auto tpl = make_tuple(1, 3.14, 665, "text");

    auto printer = [](const auto& item) { cout << item << " "; };

    tuple_apply(printer, tpl);

    cout << "\n";
}
