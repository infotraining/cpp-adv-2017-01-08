#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

template <typename T, T v>
struct IntegralConstant
{
    static const T value = v;
};

template <bool v>
using BoolConstant = IntegralConstant<bool, v>;

using TrueType = BoolConstant<true>;
using FalseType = BoolConstant<false>;

TEST_CASE("integral constant")
{
    static_assert(IntegralConstant<bool, true>::value == true, "Error");
    static_assert(BoolConstant<true>::value == true, "Error");
    static_assert(TrueType::value == true, "Error");
    static_assert(FalseType::value == false, "Error");
}

////////////////////////
///  IsVoid
///
template <typename T>
struct IsVoid : FalseType
{};

template <>
struct IsVoid<void> : TrueType
{};

// C++14 - variable templates
template <typename T = double>
constexpr T pi = 3.141552353453245634645645;

double x = pi<double>;
float y = pi<float>;
double z = pi<>;

// C++17 - type traits with variable templates
template <typename T>
constexpr bool IsVoid_v = IsVoid<T>::value;

template <typename T>
void foo(T* ptr)
{
    static_assert(IsVoid_v<T> == false, "T can't be void");

    cout << "foo(" << *ptr << ")" << endl;
}

TEST_CASE("trait: is void")
{
    static_assert(IsVoid<int>::value == false, "Error");
    static_assert(IsVoid<string>::value == false, "Error");
    static_assert(IsVoid<const int*>::value == false, "Error");
    static_assert(IsVoid<void>::value == true, "Error");
    static_assert(IsVoid_v<void> == true, "Error");

    void* ptr = nullptr;
    //foo(ptr); // compile error
}


/////////////////////////////////
/// IsPointer
///
template <typename T>
struct IsPointer : FalseType
{};

template <typename T>
struct IsPointer<T*> : TrueType
{};


TEST_CASE("trait: is_pointer")
{
    static_assert(IsPointer<int>::value == false, "Error");
    static_assert(IsPointer<string*>::value == true, "Error");
    static_assert(IsPointer<const int**>::value == true, "Error");
}
