#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <cassert>


using namespace std;

class Bitmap
{
	string name_ = "unknown";
	char* image_ = nullptr;
    size_t size_ = 0;

public:
    Bitmap(const string& name, size_t size, char fill_character = '+') : name_(name), size_{size}
	{
		assert(size >= 1);

		cout << "Ctor Bitmap(" << name_ << ")" << endl;
		image_ = new char[size];
		fill(image_, image_ + size - 1, fill_character);
		image_[size - 1] = 0;
	}

    Bitmap(const Bitmap& other) : name_{other.name_}, image_{new char[other.size_]}, size_{other.size_}
    {
        copy(other.image_, other.image_ + size_, image_);
        cout << "CopyCtor Bitmap(" << name_ << ")" << endl;
    }

    Bitmap& operator=(const Bitmap& other)
    {
        Bitmap temp(other);
        swap(temp);

        cout << "CopyOp= Bitmap(" << name_ << ")" << endl;

        return *this;
    }

    Bitmap(Bitmap&& other) noexcept : name_{move(other.name_)}, image_{move(other.image_)}, size_{move(other.size_)}
    {
        other.image_ = nullptr;
        other.size_ = 0; // not-mandatory

        cout << "MoveCtor Bitmap(" << name_ << ")" << endl;
    }

    Bitmap& operator=(Bitmap&& other)
    {
        if (&other != this)
        {
            name_ = move(other.name_);
            image_ = move(other.image_);
            size_ = move(other.size_);

            other.image_ = nullptr;
            other.size_ = 0; // not-mandatory
        }

        cout << "MoveOp= Bitmap(" << name_ << ")" << endl;

        return *this;
    }

	~Bitmap()
	{
		cout << "Dtor Bitmap(" << name_ << ")" << endl;
		delete[] image_;
	}

    void swap(Bitmap& other)
    {
        name_.swap(other.name_);
        std::swap(image_, other.image_);
        std::swap(size_, other.size_);
    }

	void draw() const	
	{
		cout << "Drawing " << name_ << ": ";

		if (image_)
			cout << image_ << endl;
	}
};

Bitmap create_bitmap()
{
	static int gen_id;

    Bitmap bmp{ "bmp_from_factory" + to_string(++gen_id), 20, '*' };

	return bmp;
}

int main()
{
    Bitmap bmp{"bmp0", 10, '^'};

    vector<Bitmap> bitmaps;
    bitmaps.push_back(move(bmp));
    cout << "\n----\n";
    bitmaps.push_back(Bitmap{"bmp1", 10});
    cout << "\n----\n";
    bitmaps.push_back(create_bitmap());
}
