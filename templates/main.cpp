#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

template <typename T>
const T& maximum(const T& a, const T& b)
{
    return a > b ? a : b;
}

// specialization (overloading) for const char*
const char* maximum(const char* a, const char* b)
{
    return strcmp(a, b) > 1 ? a : b;
}

template <typename T, size_t N>
T* begin_of_array(T(&arr)[N])
{
    return arr;
}

template <typename T, size_t N>
T* end_of_array(T(&arr)[N])
{
    return arr + N;
}

template <typename T, size_t N = 256>
struct Array
{
    T array_[N];

    using iterator = T*;
    using const_iterator = const T*;

    size_t size() const
    {
        return N;
    }

    iterator begin()
    {
        return array_;
    }

    iterator end()
    {
        return array_ + N;
    }

    const_iterator begin() const
    {
        return array_;
    }

    const_iterator end() const
    {
        return array_ + N;
    }

    T& operator[](size_t index)
    {
        return array_[index];
    }

    const T& operator[](size_t index) const;
};

template <typename T, size_t N>
const T& Array<T, N>::operator[](size_t index) const
{
    return array_[index];
}

template <typename T, template<typename,size_t> class Container, size_t N = 1024>
class Stack
{
    Container<T, N> stack_ = {};
    size_t index_ = 0;
public:
    Stack() = default;

    template <size_t M>
    Stack& operator=(const Stack<T, Container, M>& other)
    {
        static_assert(N >= M, "destination stack is to small");

        clear();

        for(size_t i = 0; i < other.size(); ++i)
            stack_[i] = other[i];
        index_ = other.size();


        return *this;
    }

    void push(const T& item)
    {
        stack_[index_] = item;
        ++index_;
    }

    void push(T&& item)
    {
        if (index_ >= N)
            throw std::out_of_range("Stack overflow");

        stack_[index_] = move(item);
        ++index_;
    }

    T& operator[](size_t index)
    {
        return stack_[index];
    }

    const T& operator[](size_t index) const
    {
        return stack_[index];
    }

    T& top()
    {
        return stack_[index_-1];
    }

    const T& top() const
    {
        if (index_ == 0)
            throw std::out_of_range("Empty stack");

        return stack_[index_-1];
    }

    void pop()
    {
        if (index_ == 0)
            throw std::out_of_range("Empty stack");

        stack_[index_-1].~T();
        --index_;
    }

    size_t size() const
    {
        return index_;
    }

    void clear()
    {
        //if constexpr(!is_trivially_destructible<T>::value)
        {
            for(size_t i = 0; i < index_; ++i)
                stack_[i].~T();
        }

        index_ = 0;
    }
};

TEST_CASE("template functions")
{
    SECTION("instance for ints")
    {
        int a = 10;
        int b = 20;

        REQUIRE(maximum(a, b) == 20);
    }

    SECTION("instance for double")
    {
        double x = 2.234;
        double pi = 3.14;

        REQUIRE(maximum(x, pi) == 3.14);
    }

    SECTION("when type deduction can fail")
    {
        SECTION("cast is required")
        {
            REQUIRE(maximum(static_cast<double>(3), 3.14) == 3.14);
        }

        SECTION("explicit template call is required")
        {
            REQUIRE(maximum<double>(3, 3.14) == 3.14);
        }
    }

    SECTION("maximum for c-string requires specialization")
    {
        REQUIRE(strcmp(maximum("ala", "ola"), "ola") == 0);
    }
}

TEST_CASE("type deduction")
{
    int tab[10];

    REQUIRE(begin_of_array(tab) == &tab[0]);
    REQUIRE(end_of_array(tab) == tab + 10);
}

TEST_CASE("class template type deduction")
{
    SECTION("before")
    {
        pair<int, double> p{1, 3.14};
    }

    SECTION("cpp17")
    {
        //pair p{1, 3.14};
    }
}

TEST_CASE("using an array")
{
    int tab[10] = { 1, 2, 3 };
    Array<int, 10> arr = { { 4, 6, 10 } };

    REQUIRE(arr.size() == 10);

    copy(arr.begin(), arr.end(), begin(tab));

    Array<double> numbers;

    REQUIRE(numbers.size() == 256);
}

TEST_CASE("stack")
{
    Stack<string, Array> stack;

    SECTION("push")
    {
        stack.push("1");
        stack.push("2");

        REQUIRE(stack.size() == 2);
        REQUIRE(stack.top() == "2");

        SECTION("pop")
        {
            stack.pop();

            REQUIRE(stack.size() == 1);
            REQUIRE(stack.top() == "1");
        }
    }

    SECTION("stack of ints")
    {
        Stack<int, array> numbers;

        numbers.push(1);
        REQUIRE(numbers.top() == 1);
        numbers.pop();
        REQUIRE(numbers.size() == 0);
    }

    SECTION("assignments")
    {
        Stack<int, array, 32> s1;

        s1.push(1);
        s1.push(2);

        Stack<int, array, 64> s2;

        s2 = s1;

        REQUIRE(s2.top() == 2);
    }
}


namespace Cpp03
{
    template <typename Container>
    typename Container::reference get_item(Container& c, size_t index)
    {
        typename Container::iterator it = c.begin();

        for(size_t i = 0; i <= index; ++i)
        {
            ++it;
        }

        return *it;
    }
}

template <typename Container>
decltype(auto) get_item(Container& c, size_t index)
{
    auto it = begin(c);
    advance(it, index);

    return *it;
}

TEST_CASE("get_item finds element in container by index")
{
    vector<int> vec = { 1, 2, 3, 4, 5 };
    get_item(vec, 2) = 6;
    REQUIRE(get_item(vec, 2) == 6);
}
