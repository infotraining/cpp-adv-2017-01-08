#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

template <typename Enum>
struct HasBitmaskOperators : false_type
{};

enum class Bitfield : uint8_t {
    field1 = 1,
    field2 = 2,
    field3 = 4,
    field4 = 8,
};

template <>
struct HasBitmaskOperators<Bitfield> : true_type
{};

enum class Coffee
{
    espresso = 1,
    cappucino = 2,
    latte = 4,
    chemex = 8
};

template <>
struct HasBitmaskOperators<Coffee> : true_type
{};


template <typename Enum, typename = enable_if_t<HasBitmaskOperators<Enum>::value>>
constexpr Enum operator |(Enum lhs, Enum rhs)
{
    using Underlying = std::underlying_type_t<Enum>;

    return static_cast<Enum>(static_cast<Underlying>(lhs) | static_cast<Underlying>(rhs));
}

template <typename Enum>
constexpr Enum operator &(Enum lhs, Enum rhs)
{
    using Underlying = std::underlying_type_t<Enum>;

    return static_cast<Enum>(static_cast<Underlying>(lhs) & static_cast<Underlying>(rhs));
}

TEST_CASE("binary operators for enum classes")
{
    SECTION("without templates")
    {
        Bitfield result =
                static_cast<Bitfield>(
                    static_cast<uint8_t>(Bitfield::field1) | static_cast<uint8_t>(Bitfield::field3));
    }

    SECTION("with templates")
    {
        Bitfield result = Bitfield::field1 | Bitfield::field3;

        Coffee c = Coffee::espresso & Coffee::cappucino;
    }
}
