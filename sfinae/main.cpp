#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <list>
#include <type_traits>

using namespace std;
using namespace Catch::Matchers;

// like std::enable_if
template <bool Condition, typename T = void>
struct EnableIf
{
    using type = T;
};

template <typename T>
struct EnableIf<false, T>
{};


// C++14 - std::enable_if_t
template <bool Condition, typename T = void>
using EnableIf_t = typename EnableIf<Condition, T>::type;

template <typename T>
EnableIf_t<(sizeof(T)<=8)> do_stuff(T obj)
{
    cout << "do_stuff(small obj)" << endl;
}

template <typename T>
EnableIf_t<(sizeof(T)>8)> do_stuff(const T& obj)
{
    cout << "do_stuff(large obj)" << endl;
}

TEST_CASE("do stuff for small object")
{
    int a = 1;
    do_stuff(a);
}

TEST_CASE("do stuff for large object")
{
    vector<int> v;
    do_stuff(v);
}

struct Shape
{
public:
    virtual size_t size() const = 0;
    virtual ~Shape() = default;
};


struct Rect : Shape
{
public:
    size_t size() const override
    {
        return 42;
    }
};


template <typename T>
using IsShape = enable_if_t<is_base_of<Shape, T>::value>;

template <typename T, typename = IsShape<T>>
void do_stuff_with_shape(const T& shp)
{
    cout << "using shape: " << shp.size() << endl;
}

TEST_CASE("do stuff with shapes")
{
    vector<int>  v;

    // do_stuff_with_shape(v); // enable_if disables a call

    Rect rect;
    do_stuff_with_shape(rect);


}


/////////////////////////////////////////////
/// Exercise
///

template <typename T>
constexpr bool IsMemCopyable_v = is_trivially_copyable<T>::value;

template <typename InIter, typename OutIter>
void mcopy(InIter start, InIter end, OutIter dest)
{
    for(auto it = start; it != end; ++it, ++dest)
    {
        *dest = *it;
    }

    cout << "Generic mcopy" << endl;
}

// optimized version
template <typename T, typename = enable_if_t<IsMemCopyable_v<T>>>
void mcopy(T* start, T* end, T* dest)
{
    auto length = end - start;
    memcpy(dest, start, length * sizeof(T));

    cout << "Optimized mcopy" << endl;
}

TEST_CASE("mcopy")
{
    SECTION("generic")
    {
        vector<int> vec = { 1, 2, 3, 4, 5 };
        list<int> lst(5);

        mcopy(vec.begin(), vec.end(), lst.begin());
    }

    SECTION("generic for array of strings")
    {
        const string words[] = { "1", "2", "3" };
        string dest[3];

        mcopy(begin(words), end(words), begin(dest));
    }

    SECTION("omptimized for arrays of POD types")
    {
        int tab1[5] = { 1, 2, 3, 4, 5 };
        int tab2[5];

        mcopy(begin(tab1), end(tab1), begin(tab2));
    }
}
