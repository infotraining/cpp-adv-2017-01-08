#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <numeric>

using namespace std;
using namespace Catch::Matchers;

///////////////////////////////////////////////////////////////////////////////
///  Accumulation Traits
///
template <typename T>
struct AccumulationTraits
{
    using AccumulatorType = T;
    static constexpr AccumulatorType const zero{};
};

template <>
struct AccumulationTraits<uint8_t>
{
    using AccumulatorType = unsigned int;
    static constexpr AccumulatorType const zero = 0;
};

template <>
struct AccumulationTraits<int>
{
    using AccumulatorType = long long int;
    static constexpr AccumulatorType const zero = 0;
};

template <>
struct AccumulationTraits<float>
{
    using AccumulatorType = double;
    static constexpr AccumulatorType const zero = 0.0f;
};

template <>
struct AccumulationTraits<const char*>
{
    using AccumulatorType = std::string;
    static const AccumulatorType zero;
};

const string AccumulationTraits<const char*>::zero = "str: ";


///////////////////////////////////////////////////////////////////////////////
/// policies
///
struct Sum
{
    template <typename TResult, typename TArg>
    static void accumulate(TResult& result, const TArg& value)
    {
        result += value;
    }
};

struct SumWithComa
{
    template <typename TArg>
    static void accumulate(string& result, const TArg& value)
    {
        result += value + ";"s;
    }
};


///////////////////////////////////////////////////////////////////////////////
/// my_accumulate
///
template <
        typename Iter,
        typename AccumulationPolicy = Sum,
        typename Traits = AccumulationTraits<remove_reference_t<decltype(*(declval<Iter>()))>>>
typename Traits::AccumulatorType my_accumulate(Iter begin, Iter end)
{
    using AccT = typename Traits::AccumulatorType;
    AccT total = Traits::zero;

    while (begin != end)
    {
        AccumulationPolicy::accumulate(total, *begin);  // instead of: total += *begin;
        ++begin;
    }

    return total;
}


TEST_CASE("accumulate values from an array")
{
    int tab[] = { 1, 2, 3 };

    REQUIRE(my_accumulate(begin(tab), end(tab)) == 6);
}

TEST_CASE("accumulate values from an vector")
{
    vector<int> vec = { 1, 2, 3 };

    REQUIRE(my_accumulate(begin(vec), end(vec)) == 6);
}

TEST_CASE("accumulate c-strings")
{
    const char* words[] = { "one", "two", "three" };

    SECTION("with default policy")
    {
        REQUIRE(my_accumulate(begin(words), end(words)) == "str: onetwothree");
    }

    SECTION("with coma policy")
    {
        auto result = my_accumulate<const char**, SumWithComa>(begin(words), end(words));

        REQUIRE(result == "str: one;two;three;");
    }
}

TEST_CASE("std::accumulate")
{
    vector<int> vec = { 1, 2, 3 };

    auto result = accumulate(vec.begin(), vec.end(), 0.0);

    static_assert(is_same<decltype(result), double>::value, "Error");
}
