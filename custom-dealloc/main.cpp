#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <memory>

const char* get_line()
{
	static unsigned int count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

void save_to_file(const char* file_name)
{
	FILE* file = fopen(file_name, "w");

	if (!file)
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for(size_t i = 0; i < 100; ++i)
		fprintf(file, get_line());

	fclose(file);
}

void save_to_file_with_unique_ptr(const char* file_name)
{
    auto file_closer = [](FILE* f) {
        fclose(f);
        std::cout << "Closing a file..." << std::endl;
    };

    //std::unique_ptr<FILE, int(*)(FILE*)> file{fopen(file_name, "w"), &fclose};
    std::unique_ptr<FILE, decltype(file_closer)> file{fopen(file_name, "w"), file_closer };

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

void save_to_file_with_shared_ptr(const char* file_name)
{
    auto file_closer = [](FILE* f) {
        if (f)
            fclose(f);
        std::cout << "Closing a file..." << std::endl;
    };

    std::shared_ptr<FILE> file{fopen(file_name, "w"), file_closer };

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

int main()
try
{
    //save_to_file("text.txt");
    //save_to_file_with_unique_ptr("text.txt");
    save_to_file_with_shared_ptr("text.txt");
}
catch(const std::exception& e)
{
	std::cout << e.what() << std::endl;

	std::cout << "Press a key..." << std::endl;
    std::string temp;
    std::getline(std::cin, temp);
}
