#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

class Lambda_473254723547
{
public:
    void operator()(int x) const
    {
        cout << "x: " << endl;
    }
};

TEST_CASE("lambdas")
{
    SECTION("can be stored in auto variable")
    {
        auto l1 = [](int x) { cout << "x: " << x << endl; return x * 2; };

        REQUIRE(l1(7) == 14);
    }

    SECTION("when lambda doesn't capture it can be stored in funtion pointer")
    {
        int(*ptr_fun)(int) = [](int x) { cout << "x: " << x << endl; return x * 2; };

        REQUIRE(ptr_fun(9) == 18);
    }
}

void foo(int x)
{
    cout << "foo(" << x << ")\n";
}

struct Foo
{
    void operator()(int x)
    {
        cout << "Foo::operator()(" << x << ")\n";
    }
};

TEST_CASE("std::function")
{
    function<void(int)> f = &foo;
    f(10);

    f = Foo();
    f(20);

    string name = "foo_lambda";
    f = [name](int x) { cout << name << "(" << x << ")\n"; };
    f(40);
}

class Printer
{
    using EventHandler = function<void(string)>;
    EventHandler on_event_;
public:
    void on()
    {
        if (on_event_)
            on_event_("Pritnter "s + to_string(reinterpret_cast<unsigned long>(this)) + " is on");
    }

    void set_on_handler(EventHandler handler)
    {
        on_event_ = handler;
    }
};

TEST_CASE("lambda with stl")
{
    auto s1 = "text";
    auto s2 = "text"s;

    vector<int> vec = { 1, 2, 3, 4, 5, 6 };

    auto is_even = [](int x) { return x % 2 == 0; };

    cout << "evens numbers: " << count_if(vec.begin(), vec.end(), is_even) << endl;

    vector<int> evens;
    vector<int> odds;

    partition_copy(vec.begin(), vec.end(), back_inserter(evens), back_inserter(odds),
                   is_even);

    cout << "evens: ";
    for(const auto& item : evens)
        cout << item << " ";
    cout << endl;

    cout << "odds: ";
    for(const auto& item : odds)
        cout << item << " ";
    cout << endl;
}

TEST_CASE("Printer")
{
    int counter = 0;

    Printer prn;

    prn.set_on_handler([&counter](string msg) { cout << "Logging: " << msg << endl; ++counter; });


    prn.on();
    prn.on();

    REQUIRE(counter == 2);
}

