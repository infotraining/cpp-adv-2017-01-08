#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

int gen_id()
{
    static int counter;
    return ++counter;
}

class IGadget
{
public:
    virtual void info() = 0;
    virtual ~IGadget() = default;
};

class Gadget : public IGadget
{
    int id_ = gen_id();
    string name_ = "unknown";    
public:
    Gadget() = default;

    Gadget(int id) : id_{id}
    {}

    Gadget(int id, const string& name) : id_{id}, name_{name}
    {}

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    void info() override
    {
        cout << "Gadget(id: " << id_ << ", name: " << name_ << ")" << endl;
    }
};

string full_name(const string fn, const string& ln)
{
    return fn + " " + ln;
}

template <typename T>
class Container
{
    vector<T> items_;
public:
    T& operator[](size_t index)
    {
        return items_[index];
    }

    size_t size() const
    {
        return items_.size();
    }

    void add(const T& item)
    {
        items_.push_back(item);
    }

    void add(T&& item)
    {
        items_.push_back(move(item));
    }
};

TEST_CASE("reference binding")
{
    SECTION("C++98")
    {
        SECTION("l-value can be bound to &")
        {
            int x = 10; // l-value
            int& rx = x;
        }

        SECTION("r-value can't be bound to &")
        {
            // string& rtxt = full_name("Jan", "Kowalski"); // compilation error
        }

        SECTION("r-value can be bound to const &")
        {
            const string& ref_name = full_name("Jan", "Kowalski");

            REQUIRE(!ref_name.empty());
        }
    }

    SECTION("C++11")
    {
        SECTION("r-value can be bound to &&")
        {
            string&& rref_name = full_name("Jan", "Kowalski"); // rref_name is l-value with a type: string&&

            rref_name[0] = 'P';

            REQUIRE(rref_name == "Pan Kowalski");
        }

        SECTION("l-value can't be bound to &&")
        {
            string first_name = "Jan";
            //string&& rref_name = first_name;  // compilation error
        }
    }
}

void foo(int x)
{
    cout << "foo(x: " << x << ")\n";
}

void foo(double) = delete;

TEST_CASE("Gadget")
{
    Gadget g1{665};

    REQUIRE(g1.id() == 665);
    REQUIRE(g1.name() == "unknown");

    Gadget g2 = move(g1);
}

///////////////////////////////////
/// Perfect forwarding
///

template <typename... Args>
std::unique_ptr<Gadget> create_gadget(Args&&... args)
{
    return std::unique_ptr<Gadget>(new Gadget{std::forward<Args>(args)... });
}

TEST_CASE("creating gadgets")
{
    auto g1 = create_gadget(1, string("ipad"));
}
